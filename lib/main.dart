import 'package:buses_task/Bloc/BusesBloc/buses_bloc.dart';
import 'package:buses_task/Theme/theme_1.dart';
import 'package:buses_task/data/datasource.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'View/HomsScreen/homeScreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<BusBloc>(
      create: (context) => BusBloc(busesData),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: theme_1,
        home: HomePage(),
      ),
    );
  }
}
