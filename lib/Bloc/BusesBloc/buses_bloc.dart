
import 'package:buses_task/Bloc/BusesBloc/buses_bloc_event.dart';
import 'package:buses_task/model/Bus.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BusBloc extends Bloc<BusesEvent,List<Bus>>{
  BusBloc(List<Bus> initialState) : super(initialState);
 
  @override 
  Stream<List<Bus>> mapEventToState(BusesEvent event) async *{
switch(event.eventType){

  case EventType.add:
  List<Bus> newState=List.from(state);
  newState.add(event.bus);
  yield newState;
  break;
  case EventType.delete:
    List<Bus> newState=List.from(state);
    newState.removeAt(event.index);

  yield newState;
  break;
  default:
  yield null;

}     // return super.mapEventToState(event);
    }

  
  
}