import 'package:buses_task/model/Bus.dart';

enum EventType{add,delete}

class BusesEvent{
  Bus bus;
  int index;
  EventType eventType;

  BusesEvent.add(Bus bus){
    this.bus=bus;
    this.eventType=EventType.add;
  
}
BusesEvent.delete(int index){
  this.index=index;
  this.eventType=EventType.delete;
}

}
