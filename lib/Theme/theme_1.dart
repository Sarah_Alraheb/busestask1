import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const Color secondary = Color(0xffD65C8F);

ThemeData theme_1 = ThemeData(
    inputDecorationTheme: InputDecorationTheme(
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(color: secondary),
          ),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(color: secondary),
          ),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(color: secondary),
           )),
    textTheme: GoogleFonts.robotoTextTheme(TextTheme(
        bodyText2: TextStyle(
      fontWeight: FontWeight.bold,
    ))),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style:
            ButtonStyle(backgroundColor: MaterialStateProperty.all(secondary)))

    //  textTheme: TextTheme(bodyText2: TextStyle(fontWeight: FontWeight.bold,))
    );
