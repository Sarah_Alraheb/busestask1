import 'package:buses_task/Bloc/BusesBloc/buses_bloc.dart';
import 'package:buses_task/Bloc/BusesBloc/buses_bloc_event.dart';
import 'package:buses_task/View/HomsScreen/widget/CustomRow.dart';
import 'package:buses_task/model/Bus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Transportation Company'),
          backgroundColor: Color(0xffD8B178),
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: BlocConsumer<BusBloc, List<Bus>>(
              listener: (context, state) {},
              builder: (context, state) {
                return Column(children: getColumnChildren(state));
              },
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: ElevatedButton(
          onPressed: () {
            BlocProvider.of<BusBloc>(context).add(BusesEvent.add(Bus()));
          },
          child: Icon(Icons.plus_one_outlined),
        ),
      ),
    );
  }

  List<Widget> getColumnChildren(List<Bus> buses) {
    List<Widget> busesList = [];
    for (var index = 0; index < buses.length; index++) {
      busesList.add(Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: CustomRow(
            index: index,
          )));
    }

    return busesList;
  }
}
