import 'package:buses_task/Bloc/BusesBloc/buses_bloc.dart';
import 'package:buses_task/Bloc/BusesBloc/buses_bloc_event.dart';
import 'package:buses_task/Theme/theme_1.dart';
import 'package:buses_task/data/datasource.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CustomRow extends StatefulWidget {
  final index;
  const CustomRow({Key key, @required this.index}) : super(key: key);

  @override
  _CustomRowState createState() => _CustomRowState();
}

class _CustomRowState extends State<CustomRow> {
  String typedropdownvalue = types[0];
  String fromdropdownvalue=cities[0];
    String Todropdownvalue=cities[0];

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Card(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Column(
          children: [
            Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Type',
                    overflow: TextOverflow.ellipsis,
                  ),
                  //  Spacer(),

                  Container(
                    height: 60,
                    width: width * 0.409,
                    child: DropdownButtonFormField(

                      value: typedropdownvalue,
                      items:
                          types.map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(

                          value: value,
                          child: Text(value,overflow: TextOverflow.clip, ),
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          typedropdownvalue = value;
                        });
                      },
                    ),
                  ),
                ]),
            Divider(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Bus Description'),
                Container(
                  height: 60,
                  width: width * 0.4,
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: 'Description',
                      // floatingLabelBehavior: FloatingLabelBehavior.always,
                    ),
                  ),
                )
              ],
            ),
            Divider(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('From:'),
                Container(
                  height: 60,
                  width: width * 0.4,
                  child: DropdownButtonFormField(
                    value: fromdropdownvalue,
                    items: cities.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        fromdropdownvalue = value;
                      });
                    },
                  ),
                ),
              ],
            ),
            Divider(
              height: 10,
            ),
             Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('To:'),
                Container(
                  height: 60,
                  width: width * 0.4,
                  child: DropdownButtonFormField(
                    value:Todropdownvalue,
                    items: cities.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        Todropdownvalue = value;
                      });
                    },
                  ),
                ),
              ],
            ),
            Divider(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Duration and prices'),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      height: 40,
                      width: 110,
                      padding: EdgeInsets.symmetric(vertical: 2),
                      child: TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Daily',
                          // floatingLabelBehavior: FloatingLabelBehavior.always,
                        ),
                      ),
                    ),
                    Container(
                      height: 40,
                      width: 110,
                      padding: EdgeInsets.symmetric(vertical: 2),
                      child: TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Weekly',
                          // floatingLabelBehavior: FloatingLabelBehavior.always,
                        ),
                      ),
                    ),
                    Container(
                      height: 40,
                      width: 110,
                      padding: EdgeInsets.symmetric(vertical: 2),
                      child: TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Monthly',
                          // floatingLabelBehavior: FloatingLabelBehavior.always,
                        ),
                      ),
                    ),
                    Container(
                      height: 40,
                      width: 110,
                      padding: EdgeInsets.symmetric(vertical: 2),
                      child: TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Yearly',
                          // floatingLabelBehavior: FloatingLabelBehavior.always,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
            Divider(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text('Upload photo'), Icon(Icons.photo_library_sharp,color:Color(0xffD65C8F),)],
            ),
            Divider(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Action'),
                IconButton(
                  icon: Icon(
                    Icons.cancel,
                    color: Colors.red,
                  ),
                  onPressed: () {
                    BlocProvider.of<BusBloc>(context)
                        .add(BusesEvent.delete(widget.index));
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
